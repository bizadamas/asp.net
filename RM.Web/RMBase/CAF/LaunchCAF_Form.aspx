﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaunchCAF_Form.aspx.cs" Inherits="RM.Web.RMBase.CAF.Lunch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CAF审批</title>
    <style type="text/css">
       
    </style>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">

        <asp:Button ID="BtnSend" runat="server" Text="发送" />
        <div class="div-frm" style="height: 400px;">

            <table id="table1" border="1px" cellpadding="0" cellspacing="0" class="frm">
                <tr>
                    <th style="width: 120px;">Entity
                    </th>
                    <td style="width: 250px;">
                        <select id="SltEntiy" class="select" runat="server" style="width: 100%;">
                            <option value="0">约克中国商贸有限公司</option>
                            <option value="1">约克中国商贸有限公司北京分公司</option>
                            <option value="2">约克中国商贸有限公司广州分公司</option>
                            <option value="3">GZF约克广州空调冷冻设备有限公司</option>
                            <option value="4">WXF约克(无锡)空调冷冻设备有限公司</option>
                            <option value="5">Meiyue</option>
                            <option value="6">JC Beijing</option>
                            <option value="7">JC Shanghai</option>
                            <option value="8">JC BJ-GZ</option>
                            <option value="9">JC BJ-SZ</option>
                            <option value="10">JC HongKong</option>
                            <option value="11">YINAL</option>
                            <option value="12">SDC</option>
                        </select>
                    </td>
                    <th style="width: 120px;">LOB
                    </th>
                    <td style="width: 250px;">
                        <select id="SltLOB" class="select" runat="server" style="width: 100%;">
                            <option value="0">ESG</option>
                            <option value="1">UPG</option>
                            <option value="2">Refrigeration</option>
                            <option value="3">System Product</option>
                            <option value="4">Channel Product</option>
                            <option value="5">Service-Equipment</option>
                            <option value="6">Service-L&amp;M</option>
                            <option value="7">Service-O&amp;M</option>
                            <option value="8">Service-Parts Only</option>
                            <option value="9">Service-PSA</option>
                            <option value="10">Service-PC</option>
                            <option value="11">Service-PR</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Region
                    </th>
                    <td>
                        <select id="SltRegion" class="select" runat="server" style="width: 100%;">
                            <option value="0">Eastern</option>
                            <option value="1">Southern</option>
                            <option value="2">Northern</option>
                            <option value="3">Center</option>
                            <option value="4">Western</option>
                            <option value="5">Southeast</option>

                        </select>
                    </td>
                    <th>Job Location
                    </th>
                    <td>
                        <input id="TxtJobLocation" runat="server" type="text" class="txt"
                            style="width: 99%;" />
                    </td>
                </tr>
                <tr>
                    <th>Branch
                    </th>
                    <td></td>

                    <th>SAP No
                    </th>
                    <td>
                        <input id="TxtSAPNo" runat="server" type="text" class="txt" style="width: 99%;" />
                    </td>
                </tr>
                <tr>

                    <th>Job Name
                    </th>
                    <td>
                        <input id="TxtJobName" runat="server" type="text" class="txt" style="width: 99%;" />
                    </td>
                    <th>Contract Value
                    </th>
                    <td>
                        <input id="TxtContractValue" runat="server" type="text" class="txt"
                            style="width: 99%;" />
                    </td>
                </tr>
                <tr>
                    <th>Job No.
                    </th>
                    <td>
                        <input id="TxtJobNo" runat="server" type="text" class="txt" style="width: 99%;" />
                    </td>
                    <th>Currency
                    </th>
                    <td>
                        <select id="SltCurrency" class="select" runat="server" style="width: 100%;">
                            <option value="0">RMB</option>
                            <option value="1">USD</option>
                            <option value="2">HKD</option>
                            <option value="3">EURO</option>


                        </select>
                    </td>
                </tr>

                <tr>
                    <th>eCAF Revosion
                    </th>
                    <td>
                        <input id="TxteCAFRevosion" runat="server" type="text" class="txt" style="width: 99%;" />

                    </td>

                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th>Attachment1
                    </th>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <th>Attachment2
                    </th>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <th>Job Information
                    </th>
                    <td colspan="3">
                        <textarea id="TxtJobInformation" class="txtRemark" runat="server" style="width: 99%; height: 60px;"></textarea>
                    </td>
                </tr>
            </table>

        </div>


    </form>
</body>
</html>
